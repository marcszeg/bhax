Üdvözli a TinyScheme
Copyright (c) Dimitrios Souflis
Script-Fu-konzol - Interaktív Scheme-fejlesztés

> (define (fakt n)
        (define (iter product counter)
                (if (> counter n)
                        product
                        (iter (* counter product) (+ counter 1))
                ))
        (iter 1 1))

(display (fakt 6))
fakt24#t
